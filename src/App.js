import './App.css';
import {Container} from 'react-bootstrap';
import {useState, useEffect} from 'react';
// importing AppNavBar function from the AppNavBar
import AppNavBar from './components/AppNavBar.js';
// import Banner from './Banner.js';
// import Highlights from './Highlights.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Error from './pages/Error.js';
import CourseView from './components/CourseView.js';

// import modules from react-router-dome for the routing


// import the UserProvider
import {UserProvider} from './UserContext.js';


import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import Logout from './pages/Logout.js';


function App() {

  const [user, setUser] = useState(null);

  useEffect(() => {
    console.log(user);
  }, [user]);

  const unSetUser = () => {
    localStorage.clear();
  }


  useEffect(()=> {
    fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {
      console.log(data);

      if(localStorage.getItem('token') !== null){
        setUser ({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      else{
        setUser(null);
      }
    })

  }, [])



  // Storing information in a Context Object is done by providing the information using the corresponding "Provider" and passing information thru the prop value.

  // All information/data provided to the Providercomponent can be access later on from the context object properties

  return (
    <Router>
      <UserProvider value = {{user, setUser, unSetUser}}>
          <AppNavBar/>
          <Routes>
            <Route path = "/" element = {<Home/>} />
            <Route path = "/courses" element = {<Courses/>} />
            <Route path = "/login" element = {<Login/>} />
            <Route path = "/register" element = {<Register/>} />
            <Route path = "/logout" element = {<Logout/>} />
            <Route path = "/course/:courseId" element = {<CourseView/>} />
            <Route path = '*' element = {<Error/>} />
          </Routes>
      </UserProvider>
    </Router>
  );
}

export default App;
