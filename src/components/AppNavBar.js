// Import bootstrap classes

import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

// Link will be used for Linking Pages to another Page and NavLink for Linking Pages in the NavBar
import {Link, NavLink} from 'react-router-dom';
import {useContext, Fragment} from 'react';
import UserContext from '../UserContext.js';


export default function AppNavBar(){
	// If you want to get the item in our localstorage, you can use .getITem("propertyToGet");
	console.log(localStorage.getItem("email"));

	// let us create a new state for the user;
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// ^^commented out kasi will be replace by UseContext

	const {user} = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
		      <Container>
		      {/*using the as keyword, NavBar.Brand inherits the properties of the link.*/}
		      {/*the to keyword links the NavBar.Brand to the declared url endpoint.*/}
		        <Navbar.Brand as = {Link} to = "/">Zuitt</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">

		          <Nav className="ms-auto">
		            <Nav.Link as = {NavLink} to = "/">Home</Nav.Link>

		            <Nav.Link as = {NavLink} to = "/courses">Courses</Nav.Link>



		            {/*Conditional Rendering*/}
		            {/*Ternary Operator: */}
		            {
		            	user ?
		            	<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
		            	:
		            	<Fragment>
		            		<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>

		            		<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
		            	</Fragment>

		            }

		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
		)

}