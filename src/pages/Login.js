import {Fragment, useState, useContext, useEffect} from 'react';
import {Button, Form, Row, Col} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';

// import sweetalert2
import Swal from 'sweetalert2';

export default function Login(){

// States
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const navigate = useNavigate();

	// const [user, setUser] = useState(localStorage.getItem('email'));

	// ^^commented out dahil di na gagamitin. We will get these info from UserContext.js


	// Allows us to consume the UserContext object and its properties for user validation.

	const {user, setUser} = useContext(UserContext);

	console.log(user);


	function login(event){
		event.preventDefault();

		// If you want to add the email of the authenticated user in the local storage:
			// Syntax:
				// localStorage.setItem("propertyName", value)


		// S55 - Process a fetch request to corresponding backend API
			// Syntax:
				// fetch('url', {options});

		fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
			method: 'POST',
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);

			if(data === false){
				Swal.fire({
					title: "Authentication failed!",
					icon: 'error',
					text: 'Please try again'
				})
			}
			else{
				localStorage.setItem('token', data.auth);
				retriveUserDetails(localStorage.getItem('token'));

				Swal.fire({
					title: "Authentication successful",
					icon: 'success',
					text: "Welcome to Zuitt!"
				})

				navigate('/');
			}
		})


		/*localStorage.setItem("email", email);
		setUser(localStorage.getItem("email"));
		alert("Logged in successfully!");
		setEmail('');
		setPassword('');
		navigate("/");*/
	}


	const retriveUserDetails = (token) => {
		
		// the token sent as part of the request'sheader information

		fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);

			setUser ({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}



// Return
	return(
		user?
		<Navigate to = "/*" />
		:
		<Row className = "mt-5">
			<Col className = 'col-md-6 col-10 mx-auto bg-success p-3'>
				<h1 className = "text-center mt-5">Login</h1>
				<Form className = "mt-5" /*onSubmit = {event => register(event)}*/>
				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control
				        	type="email"
				        	placeholder="Enter email"
				        	value = {email}
				        	onChange = {event => setEmail(event.target.value)}
				        	required
				        	 />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword">
				        <Form.Label>Password</Form.Label>
				        <Form.Control
				        	type="password"
				        	placeholder="Password"
				        	value = {password}
				        	onChange = {event => setPassword(event.target.value)}
				        	required
				        	 />
				      </Form.Group>

				      <Button variant = "primary" type = "submit" onClick = {login}>Login</Button>
				</Form>
			</Col>
		</Row>

		)
}