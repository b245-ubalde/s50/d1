import {Navigate} from "react-router-dom";
import {useContext, useEffect} from "react";
import UserContext from "../UserContext.js";




export default function Logout(){

	// localStorage.clear();
	// ^^ commented out dahil

	const {unSetUser, setUser} = useContext(UserContext);


	useEffect( () =>{
		unSetUser();
		setUser(null);
	}, []) //<=== empty array kasi magttake effect lang siya kapag na-mount na siya


	return(
		<Navigate to = "/login" />
		)
}