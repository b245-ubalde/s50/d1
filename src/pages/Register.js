import {Button, Form} from 'react-bootstrap';
import {Fragment} from 'react';

// import the hooks that are neededin our page
import {useState, useEffect, useContext} from 'react';

import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function Register(){
	// Create 3 new states where we will store the value from the input of the email,password, and confirmPassword

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	// const [confirmPassword, setConfirmPassword] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");

	// const [user, setUser] = useState(localStorage.getItem("email"));
	// ^^commented out dahil kukunin na sa UserContext yung value.

	
	const navigate = useNavigate();
	const {user, setUser} = useContext(UserContext);


	// Create another state for the button
	const [isActive, setIsActive] = useState(false);


	/*useEffect(() => {
		console.log(email);
		console.log(password);
		console.log(confirmPassword);
	}, [email, password, confirmPassword])*/

	/*useEffect(() => {
		if(email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword ){

			setIsActive(true);
		}
		else{
			setIsActive(false);
		}

	}, [email, password, confirmPassword])*/

	useEffect(() => {
		if(email !== "" && password !== "" && firstName !== "" && lastName !== "" && mobileNo !== ""){

			setIsActive(true);
		}
		else{
			setIsActive(false);
		}

	}, [email, password, firstName, lastName, mobileNo])


	function register (event){
		event.preventDefault();

		/*localStorage.setItem("email", email);
		console.log(localStorage.getItem("email"));
		setUser(localStorage.getItem("email"));
		alert("Congratulations! You are now registered on our website! Proceeding to login.");
		setEmail('');
		setPassword('');

		navigate("/");*/

		fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password,
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Successfully registered",
					icon: "success",
					text: "You can now try to login you account"
				})

				navigate('/login');
			}
			else{
				Swal.fire({
					title: "Registration failed",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}


	return(

		user?
		<Navigate to = "/*" />
		:
		<Fragment>
			<h1 className = "text-center mt-5">Register</h1>
			<Form className = "mt-5" onSubmit = {event => register(event)}>
			      <Form.Group className="mb-3" controlId="formBasicEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control
			        	type="email"
			        	placeholder="Enter email"
			        	value = {email}
			        	onChange = {event => setEmail(event.target.value)}
			        	required
			        	 />

			        <Form.Text className="text-muted">
			          We'll never share your email with anyone else.
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="formBasicPassword">
			        <Form.Label>Password</Form.Label>
			        <Form.Control
			        	type="password"
			        	placeholder="Password"
			        	value = {password}
			        	onChange = {event => setPassword(event.target.value)}
			        	required
			        	 />
			        	
			      </Form.Group>

			      {/*<Form.Group className="mb-3" controlId="formBasicConfirmPassword">
			        <Form.Label>Confirm Password</Form.Label>
			        <Form.Control
			        	type="password"
			        	placeholder="Confirm Password"
			        	value = {confirmPassword}
			        	onChange = {event => setConfirmPassword(event.target.value)}
			        	required
			        	 />
			      </Form.Group>*/}

			      <Form.Group className="mb-3" controlId="formFirstName">
			        <Form.Label>First Name</Form.Label>
			        <Form.Control
			        	type="firstName"
			        	placeholder="First Name"
			        	value = {firstName}
			        	onChange = {event => setFirstName(event.target.value)}
			        	required
			        	 />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="formLastName">
			        <Form.Label>Last Name</Form.Label>
			        <Form.Control
			        	type="lastName"
			        	placeholder="Last Name"
			        	value = {lastName}
			        	onChange = {event => setLastName(event.target.value)}
			        	required
			        	 />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="formMobileNo">
			        <Form.Label>Mobile Number</Form.Label>
			        <Form.Control
			        	type="mobileNo"
			        	placeholder="Mobile Number"
			        	value = {mobileNo}
			        	onChange = {event => setMobileNo(event.target.value)}
			        	required
			        	 />
			      </Form.Group>

			      {/*In this codeblock, we do conditional rendering on the state of our isActive*/}
			      {
			      	isActive ?
			      	<Button variant="primary" type="submit">
			      	  Submit
			      	</Button>
			      	:
			      	<Button variant="danger" type="submit" disabled>
			      	  Submit
			      	</Button>
			      }
			          
			    </Form>
		</Fragment>
		)
}