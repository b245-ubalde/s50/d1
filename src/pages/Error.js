import {Fragment} from 'react';
import {Link} from 'react-router-dom';
import {Row, Col} from 'react-bootstrap';



export default function Error(){

	return(
		<Row className = "mt-3">
			<Col className = "col-md-6 col-10 mx-auto p-3">
				<Fragment>
					<h1>Page Not Found</h1>
					<p>Go back to the <Link to = "/">homepage</Link></p>
				</Fragment>
			</Col>
		</Row>
		)
}