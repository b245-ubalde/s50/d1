// npm for node
// npx for js

// react-bootstrap bootstrap

// To create a react app,
	// npx create-react-app project-name

// To run our react application
	// npm start

	// Files to be remmoved:
		/*
			From src folder:
				App.test.js
				index.css
				reportWebVitals.js
				logo.svg

			We need to delete all the importations of the said files.
		*/

// the 'import' statement allows us to use the code/export the modules from other files similar to how we use 'require' in Nodejs

// React JS applies the concepts of Rendering and Mounting in order to display and create components

// Mounting - Display Component to Webpage
// Rendering - Invoking the function



// [SESSION 52]

// Effect Hooks in React allows us to execute a piece of code whenever a component gets rendered to the page or if the value of a state changes.
	// This is useful if we want our change to be reactive.
	/*
		Examples of Side Effects:
			- fetching data from a server OR
			- changing the contents of a page
	*/

// Kapag useState, only accessible only within that scope.
// In general, we use useState 
// useEffect - perform operation after every render.

// using the event.target.value will capture the value inputted by the user on our input area

	// in the dependencies in useEffect
	// mattrigger kapag may nabago sa values na binabantayan nila
		// 1. Single Dependency [dependency]
			// On the initial load of the component, the side effect/ function will be triggered and whenever changes occur on our dependency
		// 2. Empty Dependency
			// The side effect will only run during the initial load.
		// 3. Multiple Dependency [dependency1, dependency, .....]
			// The side effect will run during the initial load and whenever the state of the dependencies changes.




// [SESSION 53] react-router-dom
	// for use to be able to use the module/library across all of our pages, we have to contain them with BrowserRouter/Router

	// Routes - we contain all of the routes in our react-app

	// Route - specific route wherein we will declare the url andalso the component or page to be mounted.

	// Link will be used for Linking Pages to another Page.
	
	// NavLink for Linking Pages in the NavBar

	// If you want to add the email of the authenticated user in the local storage:
		// Syntax:
			// localStorage.setItem("propertyName", value)

		// localStorage.setItem("email", email);

	// The "localStorage.setItem" allows us to manipulate the browser's localStorage property to store information indefinitely.

	// If you want to get the item in our localstorage, you can use .getITem("propertyToGet")
		// console.log(localStorage.getItem("email"));




// [SESSION 54] React.js - App State Management
	// Manage and pass data within a React JS application
	

	// Link - need na may i-click
	// Navigate - conditional link
	// href - outside REACT links


// [SESSION 55] Environment Variables
	// Environment Variables are important in hiding sensitive pieces ofinformation like the backend API which can be exploited if added directly into our code.

	// Kapag params - no need maglagay ng body
	// kapag may body - need ng input



// [Wireframes, Mockups, and Prototypes] - S58

	// Wireframes are only meant to document and communicate the design.

	// Mockups - non-interactive design of the webpage

	// Prototypes - behave like a real webpage.

